{
  description = "zhoor";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { localSystem = system; };

    in {
      devShell.x86_64-linux = pkgs.mkShell {
        buildInputs = [ pkgs.gleam pkgs.erlang_nox pkgs.rebar3 ];
      };
    };
}
