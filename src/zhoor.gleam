import argv
import gleam/io
import zhoor/cache
import zhoor/relay

pub fn main() {
  case argv.load().arguments {
    ["cache"] -> cache.start()
    ["relay"] -> relay.start()
    _ -> {
      io.println("Not implemented command")
    }
  }
}
