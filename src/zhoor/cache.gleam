import birl
import gleam/bit_array as bita
import gleam/bytes_builder
import gleam/erlang/process.{type Subject}
import gleam/function
import gleam/http.{Post}
import gleam/http/request.{type Request}
import gleam/http/response.{type Response}
import gleam/int
import gleam/io
import gleam/json.{array, string, to_string}
import gleam/list
import gleam/option.{type Option, None, Some}
import gleam/otp/actor
import gleam/result
import gleam/string
import mist.{type Connection, type ResponseData}
import zhoor/incoming_request as ir

type ActorMessage(e) {
  Shutdown
  CachePush(push: ir.IncomingRequest)
  CacheClientConnected(Subject(WSClientMessage))
  CacheClientDisconnected
}

type WSClientMessage {
  SendRequestsToWSClient(List(ir.IncomingRequest))
}

type ProviderRequest {
  GitHubRequest(req: ir.IncomingRequest)
  GitLabRequest(req: ir.IncomingRequest)
}

type CacheError {
  UnknownProvider(String)
  // InvalidGitlabAuth(String)
  // InvalidGitHubAuth(String)
}

// Detect the provider type from the request hearders
// https://docs.github.com/en/webhooks/webhook-events-and-payloads#delivery-headers
fn provider_type(
  i_req: ir.IncomingRequest,
) -> Result(ProviderRequest, CacheError) {
  case
    {
      i_req.req
      |> request.get_header("User-Agent")
    }
  {
    Ok(ua) ->
      case ua |> string.split("/") {
        ["GitLab", _] -> Ok(GitLabRequest(i_req))
        ["GitHub-Hookshot", _] -> Ok(GitHubRequest(i_req))
        _ ->
          Error(UnknownProvider(
            "Incoming request does have the supported 'User-Agent'",
          ))
      }
    Error(_) ->
      Error(UnknownProvider(
        "Incoming request does not have a 'User-Agent' header",
      ))
  }
}

// Validate the authenticity of the request
fn validate_authenticity(
  r: ProviderRequest,
) -> Result(ir.IncomingRequest, CacheError) {
  case r {
    // TODO: Implement the validation
    GitLabRequest(ir) -> Ok(ir)
    // TODO: Implement the validation
    GitHubRequest(ir) -> Ok(ir)
  }
}

pub fn start() {
  let assert Ok(cache) = actor.start(#(None, []), cache_handle)

  // For the WebSocket
  let ws_client_state = cache

  let handle_payload = fn(request: Request(Connection), conn_name: String) -> Response(
    ResponseData,
  ) {
    mist.read_body(request, 1024 * 1024 * 10)
    // Content length or 10 MBytes max
    |> result.map(fn(req) {
      case req.method {
        Post -> {
          case
            {
              ir.IncomingRequest(
                birl.utc_now() |> birl.to_unix_milli,
                conn_name,
                req,
              )
              |> provider_type()
              |> result.then(validate_authenticity)
            }
          {
            Ok(ir) -> process.send(cache, CachePush(ir))
            Error(err) -> {
              // TODO Use something else then debug
              io.debug(err)
              Nil
            }
          }
          response.new(200)
          |> response.set_body(mist.Bytes(bytes_builder.new()))
        }
        _ -> {
          io.println("Unable to handle non Post request")
          response.new(400)
          |> response.set_body(mist.Bytes(bytes_builder.new()))
        }
      }
    })
    |> result.lazy_unwrap(fn() {
      io.println("Unable to handle the request")
      response.new(400)
      |> response.set_body(mist.Bytes(bytes_builder.new()))
    })
  }
  let not_found =
    response.new(404)
    |> response.set_body(mist.Bytes(bytes_builder.new()))

  let assert Ok(_) =
    fn(req: Request(Connection)) -> Response(ResponseData) {
      case request.path_segments(req) {
        ["connection", conn_name, "payload"] -> handle_payload(req, conn_name)
        ["ws"] ->
          mist.websocket(
            request: req,
            on_init: fn(_conn) {
              io.println("New WS client connection !")
              let subject = process.new_subject()
              let selector =
                process.new_selector()
                |> process.selecting(subject, function.identity)
              process.send(ws_client_state, CacheClientConnected(subject))
              #(ws_client_state, Some(selector))
            },
            on_close: fn(_state) {
              io.println("Connection WS client disconnected !")
              process.send(ws_client_state, CacheClientDisconnected)
            },
            handler: handle_ws_message,
          )
        _ -> not_found
      }
    }
    |> mist.new
    |> mist.port(3000)
    |> mist.start_http

  process.sleep_forever()
}

fn cache_handle(
  message: ActorMessage(e),
  state: #(Option(Subject(WSClientMessage)), List(ir.IncomingRequest)),
) -> actor.Next(
  ActorMessage(e),
  #(Option(Subject(WSClientMessage)), List(ir.IncomingRequest)),
) {
  case message {
    Shutdown -> actor.Stop(process.Normal)
    CachePush(data) -> {
      let #(maybe_client, cache) = state
      io.println("Cache size: " <> cache |> list.length |> int.to_string)
      case maybe_client {
        Some(s) -> {
          process.send(s, SendRequestsToWSClient([data]))
          actor.continue(#(maybe_client, cache))
        }
        None -> {
          let new_cache = list.append(cache, [data])
          actor.continue(#(maybe_client, new_cache))
        }
      }
    }
    CacheClientConnected(s) -> {
      let #(_, cache) = state
      process.send(s, SendRequestsToWSClient(cache))
      actor.continue(#(Some(s), []))
    }

    CacheClientDisconnected -> {
      let #(_, cache) = state
      actor.continue(#(None, cache))
    }
  }
}

fn handle_ws_message(state, conn, message) {
  case message {
    mist.Custom(SendRequestsToWSClient(irs)) -> {
      let data =
        irs
        |> list.map(ir.to_ireq)
        |> array(ir.to_json)
        |> to_string
        |> bita.from_string
      let assert Ok(_) = mist.send_binary_frame(conn, data)
      actor.continue(state)
    }
    mist.Text(_) | mist.Binary(_) -> actor.continue(state)
    mist.Closed | mist.Shutdown -> actor.Stop(process.Normal)
  }
}
