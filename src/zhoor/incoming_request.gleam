import gleam/bit_array as bita
import gleam/dict.{type Dict}
import gleam/dynamic
import gleam/http/request.{type Request, Request}
import gleam/json.{type Json, int, object, string}
import gleam/list

pub type IncomingRequest {
  IncomingRequest(epoch: Int, conn_name: String, req: Request(BitArray))
}

pub type Req {
  Req(headers: Dict(String, String), body: String, path: String)
}

// This data type is used to (de)/serialize over the websocket
pub type IRequest {
  IRequest(epoch: Int, conn_name: String, req: Req)
}

pub fn to_ireq(ir: IncomingRequest) -> IRequest {
  let body = case { ir.req.body |> bita.to_string } {
    Ok(b) -> b
    Error(_) -> panic
  }
  IRequest(
    ir.epoch,
    ir.conn_name,
    Req(ir.req.headers |> dict.from_list, body, ir.req.path),
  )
}

pub fn to_json(ir: IRequest) -> Json {
  let request_to_json = fn(r: Req) -> Json {
    let headers =
      r.headers
      |> dict.to_list
      |> list.fold([], fn(acc, kv) {
        let #(k, v) = kv
        acc |> list.append([#(k, string(v))])
      })
    object([
      #("headers", object(headers)),
      #("body", string(r.body)),
      #("path", string(r.path)),
    ])
  }
  object([
    #("epoch", int(ir.epoch)),
    #("conn_name", string(ir.conn_name)),
    #("req", ir.req |> request_to_json),
  ])
}

pub fn from_json_list(
  json_string: String,
) -> Result(List(IRequest), json.DecodeError) {
  let decoder = fn() -> fn(dynamic.Dynamic) ->
    Result(IRequest, List(dynamic.DecodeError)) {
    let req_decoder =
      dynamic.decode3(
        Req,
        dynamic.field("headers", dynamic.dict(dynamic.string, dynamic.string)),
        dynamic.field("body", dynamic.string),
        dynamic.field("path", dynamic.string),
      )
    dynamic.decode3(
      IRequest,
      dynamic.field("epoch", dynamic.int),
      dynamic.field("conn_name", dynamic.string),
      dynamic.field("req", req_decoder),
    )
  }
  json.decode(json_string, dynamic.list(decoder()))
}
