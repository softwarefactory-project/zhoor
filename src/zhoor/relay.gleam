import gleam/bit_array as bita
import gleam/dict
import gleam/erlang/os
import gleam/erlang/process
import gleam/function
import gleam/hackney
import gleam/http
import gleam/http/request.{type Request, Request}
import gleam/http/response.{type Response}
import gleam/io
import gleam/list
import gleam/option.{None, Some}
import gleam/otp/actor
import gleam/result
import zhoor/incoming_request as ir

import stratus

fn to_request(ireq: ir.IRequest, target_host: String) -> Request(String) {
  Request(
    method: http.Post,
    headers: ireq.req.headers |> dict.to_list,
    body: ireq.req.body,
    scheme: http.Http,
    host: target_host,
    port: Some(8000),
    path: ireq.req.path,
    query: None,
  )
}

pub fn start() {
  let target_host = os.get_env("TARGET_HOST") |> result.unwrap("localhost")
  let cache_url =
    os.get_env("CACHE_URL") |> result.unwrap("http://localhost:3000/ws")

  let assert Ok(req) = request.to(cache_url)

  let builder =
    stratus.websocket(
      request: req,
      init: fn() { #(Nil, None) },
      loop: fn(msg, state, _conn) {
        case msg {
          stratus.Binary(data) -> {
            let irs = case { data |> bita.to_string() } {
              Ok(s) -> {
                case s |> ir.from_json_list() {
                  Ok(irs) -> irs
                  Error(decode_err) -> {
                    io.print(
                      "Unable to deserialise the payload. Skipping payload.",
                    )
                    io.debug(decode_err)
                    []
                  }
                }
              }
              Error(_) -> {
                io.print("Unable to stringify the data. Skipping payload.")
                []
              }
            }
            io.debug(
              irs
              |> list.map(fn(r) { r |> to_request(target_host) })
              |> list.map(send_request),
            )
            actor.continue(state)
          }
          stratus.Text(_) | stratus.User(_) -> actor.continue(state)
        }
      },
    )
    |> stratus.on_close(fn(_state) { io.println("oh noooo") })

  let assert Ok(subj) = stratus.initialize(builder)

  let done =
    process.new_selector()
    |> process.selecting_process_down(
      process.monitor_process(process.subject_owner(subj)),
      function.identity,
    )
    |> process.select_forever

  io.debug(#("WebSocket process exited", done))
  Nil
}

fn send_request(
  request: Request(String),
) -> Result(Response(String), hackney.Error) {
  io.debug(request)
  request |> hackney.send
}
